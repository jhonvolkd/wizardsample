import { combineReducers } from 'redux';
import { dialogs } from 'Common/Reducers';

const reducers = combineReducers({
  dialogs,
});
export default reducers;

