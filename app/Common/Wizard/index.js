import { connect } from 'react-redux';
import WizardComponent from './component';
import { registerDialog, openDialog, closeDialog } from '../Actions';

const mapStateToProps = (state) => {
  return {
    currentDialog: state.dialogs.currentDialog,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    openDialog(name) {
      dispatch(openDialog(name));
    },
    closeDialog(name) {
      dispatch(closeDialog(name));
    },
    registerDialog(name) {
      dispatch(registerDialog(name));
    },
  };
};

const Wizard = connect(mapStateToProps, mapDispatchToProps)(WizardComponent);
export default Wizard;
