/* @flow */
/* global document */

import './Wizard.less';
import React from 'react';
import ReactDOM from 'react-dom';
import StyledDialog from '../StyledDialog';
import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';
import { bindThis, on } from '../HelperMethods';

type DefaultProps = {};
type Page = {
  name: string, // the title of the dialog
  explanation: string, // a short explanation of what information the pages is asking for and why
  displayElement: React$Element<*>, // the React component to display in the wizard page
  onFocus: () =>  void, // callback to run when page is focused
};
type Props = {
  type: string, // what the dialog is a creating, e.g. "Car"
  open: boolean,
  // while the wizard is not responsible for keeping track of any data entered, it sometimes needs to know if any changes have been made
  changed: boolean,
  pages: Array<Page>, // a list of {@link Page}
  registerDialog: (name: string) => void, // keep track of this dialog in app store
  openDialog: (name: string) => void, // inform the store that this instance wants to open as soon as possible
  closeDialog: (name: string) => void, // inform the store that this instance has closed
  currentDialog: string, // the current dialog managed by the store that is open, must be true to show the wizard dialog
  onComplete: () => void, // inform the parent that the user has completed the wizard
  onCancel: () => void, // inform the parent that the user has canceled the wizard
  discardChanges: () => void, // informt the partent that the user has chosen to cancel the dialog and discard all changes
  name: string, // the internal name of dialog, should be unique
  title: string, // the tile of the wizard, e.g. "Create New Car"
};
type State = {
  pages: Array<Page>,
  currentPage: number, // index of page in pages array
  confirmCancelOpen: boolean,
};

let lastPage = 0;

/**
 * A class for displaying a series of Wizard pages.
 * Each page contains identifying information and a React Component to display within the wizard.
 * The WizardComponent class handles the progression through the pages and will notify the parent class of completion or cancellation.
 */
export default class WizardComponent extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;
  wizardActions: Array<React$Element<*>>; // the actions displayed at the bottom of the Wizard dialog.
  wizardInner: React$Component<*, *, *>;
  keyListenerSet: boolean; // has the enter key listener been set on the wizardInner Component
  confirmActions: Array<React$Element<*>>; // the actions displayed in the confirmation dialog shown when the user presses the cancel button

  constructor(props: Props) {
    super(props);
    bindThis(this);
    // register all dialogs
    props.registerDialog(this.props.name);
    this.keyListenerSet = false;
    this.state = {
      pages: props.pages,
      currentPage: -1,
      confirmCancelOpen: false,
    };
    this.confirmActions = [
      <FlatButton
        label="Continue Working"
        onClick={this.closeConfirmCancel}
      />,
      <FlatButton
        label="Discard Changes"
        onClick={this.discardChanges}
        secondary={true}
      />,
    ];
  }

  componentDidMount() {
    this.keyListenerSet = false;
  }

  componentWillMount() {
    if (this.props.open) {
      this.props.openDialog(this.props.name);
      this.setState({ currentPage: 0 });
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (this.wizardInner) {
      if (!this.keyListenerSet) {
        this.keyListenerSet = true;
        // advance the page on enter key
        on(ReactDOM.findDOMNode(this.wizardInner), 'keydown', (event: Event) => {
          if (event.key === 'Enter') {
            this.closeCurrentPage();
          }
        });
      }
    } else {
      this.keyListenerSet = false;
    }
    if (prevState.currentPage !== this.state.currentPage && this.state.currentPage >= 0 && this.state.currentPage < this.props.pages.length) {
      // if the user has navigated to a new page and the page has provided an onFocus function, refocus
      const nextPage = this.props.pages[this.state.currentPage];
      // if page has onFocus method, call it
      if (typeof nextPage.onFocus === 'function') {
        setTimeout(() => {
          this.props.pages[this.state.currentPage].onFocus();
        }, 1);
      }
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.open && !this.props.open) { // if WizardComponent is instructed to open while closed
      this.props.openDialog(this.props.name);
      let currentPage = this.state.currentPage;
      // if open is true, make sure the currentPage is set
      if (currentPage < 0) {
        currentPage = 0;
      }
      this.setState({ currentPage, pages: nextProps.pages });
    } else if (!nextProps.open && this.props.open) { // if WizardComponent is instructed to close while open
      this.props.closeDialog(this.props.name);
      this.setState({ currentPage: -1, pages: nextProps.pages });
    } else {
      // otherwise merely update the list of pages
      this.setState({ pages: nextProps.pages });
    }
  }

  /**
   * Inform the parent of completion, set this.state.currentPage to -1, and close the dialog.
   */
  cancelWizards() {
    // close the current dialog
    lastPage = this.state.currentPage;
    this.setState({ currentPage: -1 });
    this.props.closeDialog(this.props.name);
    this.onCancel();
  }

  back() {
    this.setState({ currentPage: this.state.currentPage - 1 });
  }

  /**
   * Closes the current page. If the closed page is not the last page, advance this.state.currentPage.
   * If it was the last page, inform the parent of completion, set this.state.currentPage to -1, and close the dialog.
   */
  closeCurrentPage() {
    const nextPageIndex = this.state.currentPage + 1;
    // if there are more pages
    if (nextPageIndex < this.props.pages.length) {
      // blur the active focus and give focus back to wizardInner
      document.activeElement.blur();
      this.setState((): Object => {
        return {
          currentPage: nextPageIndex,
        };
      });
      ReactDOM.findDOMNode(this.wizardInner).focus();
    } else {
      this.props.onComplete();
    }
  }

  onCancel() {
    // we only need to show the confirmation dialog if the user has made changes in a wizard page.
    if (this.props.changed) {
      this.setState({ confirmCancelOpen: true });
    } else {
      this.props.onCancel();
    }
  }

  /**
   * Closes the cancel wizard confirmation dialog.
   */
  closeConfirmCancel() {
    this.setState({ confirmCancelOpen: false, currentPage: lastPage });
    this.props.openDialog(this.props.name);
  }

  /**
   * Resets the page count to zero,
   * informs the parent to discard changes and that the wizard has been canceled,
   * and closes the cancellation confirmation dialog.
   */
  discardChanges() {
    lastPage = 0;
    this.props.discardChanges();
    this.props.onCancel();
    this.setState({ confirmCancelOpen: false });
  }


  render(): React$Element<*> {
    this.wizardActions = [
      <FlatButton
        label="Cancel"
        className="cancel"
        secondary={true}
        onTouchTap={this.cancelWizards}
      />,
      <FlatButton
        label="Back"
        disabled={this.state.currentPage <= 0}
        onTouchTap={this.back}
      />,
      <FlatButton
        label={(this.state.currentPage === this.props.pages.length - 1) ? 'Submit' : 'Next'} // change label to "Submit" on last page
        primary={true}
        onTouchTap={this.closeCurrentPage}
      />,
    ];
    const currentPage = this.state.pages[this.state.currentPage];
    let display;
    if (this.props.name === this.props.currentDialog && this.state.currentPage >= 0) {
      // if the wizard is the current dialog showing in the app and the current page is not negative
      // show the wizard information

      let explanation;
      if (currentPage.explanation && currentPage.explanation.length > 0) {
        explanation = <h2 className="explanation">{currentPage.explanation}</h2>;
      }
      display = (
        <StyledDialog
          title={this.props.title}
          modal={true}
          open={true}
          onRequestClose={this.cancelWizards}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          actions={this.wizardActions}
          contentClassName="wizard-content"
          className="wizard-dialog"
          titleBackgroundColor={'#1E88E5'}
        >
        <div className="progress-wrapper">
          <LinearProgress mode="determinate" max={this.state.pages.length - 1} value={this.state.currentPage} />
        </div>
        <div className="wizard-inner" ref={(ref: React$Component<*, *, *>) => { this.wizardInner = ref; }} tabIndex="0">
          <h1 className="wizard-page-title">{currentPage.name}</h1>
          {explanation}
          <div className="display-element">
            {currentPage.displayElement}
          </div>
        </div>
        </StyledDialog>
      );
    }
    return (
      <div>
        <StyledDialog
          title="Do you want to save a draft of your changes?"
          modal={true}
          open={this.state.confirmCancelOpen}
          onRequestClose={this.closeConfirmCancel}
          autoScrollBodyContent={true}
          closeIconInTitle={true}
          actions={this.confirmActions}
        >
          <div className="confirm-close-new-iol">
            <p>{`You have unsaved changes for this ${this.props.type}. Would you like to keep those changes as a draft?`}</p>
          </div>
        </StyledDialog>
          {display}
      </div>
    );
  }
}
