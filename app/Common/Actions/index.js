/* @flow */


/**
 * Creates a standard action creator.
 * @param  {string} type     Value of type field in {@link object} returned by action creator.
 * @param  {...string} argNames Fields in the {@link object}
 * @return {Function}          The action creator.
 */
export function makeActionCreator(type: string, ...argNames: Array<string>): (...args: Array<any>) => Object {
  return function (...args: Array<any>): Object {
    const action = { type };
    argNames.forEach((arg: any, index: any) => {
      action[argNames[index]] = args[index];
    });
    return action;
  };
}

export const OPEN_DIALOG_TYPE = 'OPEN_DIALOG_TYPE';
// export const openDialog = makeActionCreator(OPEN_DIALOG_TYPE, 'name');
let openIndex = 0;
export const openDialog = (name: string): {
type: string,
name: string,
openIndex: number } => {
  return {
    type: OPEN_DIALOG_TYPE,
    name,
    openIndex: openIndex++,
  };
};
export const REGISTER_DIALOG_TYPE = 'REGISTER_DIALOG_TYPE';
// export const registerDialog = makeActionCreator(REGISTER_DIALOG_TYPE, 'name');
export const registerDialog = (name: string): { type: string, name: string } => {
  return {
    type: REGISTER_DIALOG_TYPE,
    name,
  };
};

export const CLOSE_DIALOG_TYPE = 'CLOSE_DIALOG_TYPE';
export const closeDialog = makeActionCreator(CLOSE_DIALOG_TYPE, 'name');
