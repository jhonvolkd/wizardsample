/* @flow */
import './StyledDialog.less';

import React from 'react';
import Dialog from 'material-ui/Dialog';
import Close from 'material-ui/svg-icons/navigation/close';

export type Props = {
  /**
   * Dialog children, usually the included sub-components.
   */
  children?: Node,
  /**
   * Useful to extend the style applied to components.
   */
  classes?: Object,
  /**
   * @ignore
   */
  className?: string,
  /**
   * If true, show close icon in title.
   */
  closeIconInTitle: boolean,
  /**
   * If `true`, it will be full-screen
   */
  fullScreen?: boolean,
  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick?: boolean,
  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp?: boolean,
  /**
   * Duration of the animation when the element is entering.
   */
  enterTransitionDuration?: number, // eslint-disable-line react/sort-prop-types
  /**
   * Duration of the animation when the element is leaving.
   */
  leaveTransitionDuration?: number,
  /**
   * Determine the max width of the dialog.
   * The dialog width grows with the size of the screen, this property is useful
   * on the desktop where you might need some coherent different width size across your
   * application.
   */
  maxWidth?: 'xs' | 'sm' | 'md',
  /**
   * Callback fired when the backdrop is clicked.
   */
  onBackdropClick?: Function,
  /**
   * Callback fired before the dialog enters.
   */
  onEnter?: TransitionCallback,
  /**
   * Callback fired when the dialog is entering.
   */
  onEntering?: TransitionCallback,
  /**
   * Callback fired when the dialog has entered.
   */
  onEntered?: TransitionCallback, // eslint-disable-line react/sort-prop-types
  /**
   * Callback fires when the escape key is released and the modal is in focus.
   */
  onEscapeKeyUp?: Function, // eslint-disable-line react/sort-prop-types
  /**
   * Callback fired before the dialog exits.
   */
  onExit?: TransitionCallback,
  /**
   * Callback fired when the dialog is exiting.
   */
  onExiting?: TransitionCallback,
  /**
   * Callback fired when the dialog has exited.
   */
  onExited?: TransitionCallback, // eslint-disable-line react/sort-prop-types
  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose?: Function,
  /**
   * If `true`, the Dialog is open.
   */
  open?: boolean,
  /**
   * Transition component.
   */
  transition?: Node,
};

/**
 * Makes some styling changes to {@link Dialog} that would be impossible without a wrapper class.
 * Adds an optional close button.
 */
function StyledDialog(props: Props): React$Element<*> {
  let title = props.title;
  if (props.closeIconInTitle) {
    title = (
        <div className={`${props.titleClassName || ''} styled-dialog-title styled-dialog-title-with-close-icon`}>
          <span className="title-text">{props.title}</span>
          <div className="title-icon">
            <Close
              onClick={props.onRequestClose}
              style={
              (props.titleBackgroundColor && props.titleBackgroundColor.length > 0)
              ? {
                backgroundColor: props.titleBackgroundColor,
                color: 'white',
              }
              : {}
            }
            />
        </div>
        </div>
    );
  }

  return (
      <Dialog
        {...props}
        title={title}
        titleStyle={
          Object.assign(
            {},
            (props.titleBackgroundColor && props.titleBackgroundColor.length > 0)
            ? {
              backgroundColor: props.titleBackgroundColor,
              color: 'white',
            }
            : {},
            props.titleStyle,
          )
        }
        className={`${props.className || ''} styled-dialog`}
        actionsContainerClassName={`${props.actionsContainerClassName || ''} styled-dialog-actions-container`}
        bodyClassName={`${props.bodyClassName || ''} styled-dialog-body`}
        contentClassName={`${props.contentClassName || ''} styled-dialog-content`}
        overlayClassName={`${props.overlayClassName || ''} styled-dialog-overlay`}
        titleClassName={`${props.titleClassName || ''} styled-dialog-title`}
      >
      {props.children}
    </Dialog>
  );
}

export default StyledDialog;
