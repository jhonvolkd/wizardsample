/* @flow */

const constructorName = 'constructor';
/**
 * Binds the passed in reference _this as this to all properties in _this.
 * @param  {object}  _this class to bind this to in methods
 */
export function bindThis(_this: Object) {
  for (const name of Object.getOwnPropertyNames(Object.getPrototypeOf(_this))) {
    if (name !== constructorName) {
      _this[name] = _this[name].bind(_this);
    }
  }
}

/**
 * Sets an event handler on a {@link HTMLElement}.
 * @param  {HTMLElement} el   The {@link HTMLElement} to set the listener on.
 * @param  {string} eventName Name of event
 * @param  {Function} handler The handler function.
 * @return {void}
 */
export function on(el: HTMLElement, eventName: string, handler: (e: ?Event) => void) {
  if (typeof el !== 'undefined') {
    if (el.addEventListener) {
      el.addEventListener(eventName, handler);
    } else {
      el.attachEvent('on' + eventName, () => {
        handler();
      });
    }
  }
}
