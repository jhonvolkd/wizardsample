import {
  REGISTER_DIALOG_TYPE,
  OPEN_DIALOG_TYPE,
  CLOSE_DIALOG_TYPE,
} from '../Actions';


/**
 * A generic reducer for registering an element of a managed type.
 * There may only be one current element of a particular at a time.
 *
 * @param  {Object}  The current state.
 * @param  {String}  The name of the element.
 */
function register(state, name) {
  if (name && !state[name]) {
    const newState = Object.assign({}, state);
    newState[name] = {
      open: false,
    };
    return newState;
  }
  console.error('Attempting to re-register dialog.');
  return state;
}

/**
 * A generic reducer for opening an element of a managed type.
 * There may only be one current element of a particular at a time.
 *
 * @param  {Object}  The current state.
 * @param  {String}  The name of the element.
 * @param  {String}  The name of the property holding the current element. e.g. 'currentDialog'
 * @param  {String}  The index of the currently open element.
 */
function open(state, name, property, openIndex) {
  const newState = Object.assign({}, state);
  newState[name].open = true;
  newState[name].openIndex = openIndex;
  if (!newState[property] || newState[property].length === 0) {
    newState[property] = name;
  }
  return { state: newState, openIndex: openIndex + 1 };
}

/**
 * A generic reducer for closing an element of a managed type.
 * There may only be one current element of a particular at a time.
 *
 * @param  {Object}  The current state.
 * @param  {String}  The name of the element.
 * @param  {String}  The name of the property holding the current element. e.g. 'currentDialog'
 */
function close(state, name, property) {
  const newState = Object.assign({}, state);
  newState[name].open = false;
  let nameToOpen = '';
  let earliestIndex = -1;
  // only reset currentDialog if we are closing the currentDialog
  if (name === newState[property]) {
    // look for the open dialog with the lowest openIndex
    for (const searchName in newState) {
      if (newState.hasOwnProperty(searchName) && typeof newState[searchName] === 'object') {
        if (newState[searchName].open && (earliestIndex < 0 || newState[searchName].openIndex < earliestIndex)) {
          earliestIndex = newState[searchName].openIndex;
          nameToOpen = searchName;
        }
      }
    }
    if (earliestIndex > -1) {
      newState[property] = nameToOpen;
    } else {
      newState[property] = '';
    }
  }
  return newState;
}

let openDialogIndex = 0;

/**
 * A reducer for handling the types: REGISTER_DIALOG_TYPE, OPEN_DIALOG_TYPE, and CLOSE_DIALOG_TYPE.
 *
 * REGISTER_DIALOG_TYPE:
 * Adds a new dialog to the list in dialogs that is initial closed.
 *
 * OPEN_DIALOG_TYPE:
 * Sets the dialog named to open, but does not yet open.
 * Many dialogs can be set to open.
 * Only one is current displayed.
 * Dialogs are displayed in the order they are opened.
 *
 * CLOSE_DIALOG_TYPE:
 * Sets the dialog to closed.
 * If the currentDialog is closed, set the new currentDialog to the open dialog
 * with the lowest openIndex.
 *
 * @param  {Object} [state={ currentDialog: null }] The current state.
 * @param  {Object} action  The current action.
 * @return {Object}         The new state.
 */
export const dialogs = (state = { currentDialog: '' }, action) => {
  switch (action.type) {
    case REGISTER_DIALOG_TYPE: {
      return register(state, action.name);
    }
    case OPEN_DIALOG_TYPE: {
      const openReturn = open(state, action.name, 'currentDialog', openDialogIndex);
      openDialogIndex = openReturn.openIndex;
      return openReturn.state;
    }
    case CLOSE_DIALOG_TYPE: {
      return close(state, action.name, 'currentDialog');
    }
    default:
      return state;
  }
};
