/* globals document */
/* @flow */
import './WizardDisplay';
import React from 'react';
import ReactDOM from 'react-dom';
import Wizard from 'Common/Wizard';
import StyledDialog from 'Common/StyledDialog';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { bindThis } from 'Common/HelperMethods';

type DefaultProps = {};

type Props = {};

type State = {
  changed: boolean,
  open: boolean,
  submittedOpen: boolean,
  name: string, // the name of the new Starship e.g. "Enterprise"
  className: string, // the class of the new Starship e.g. "Constitution"
  registry: string, // the registry of the new Starship e.g. "NCC-1701"
};

export default class WizardDisplayComponent extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;

  constructor(props: Props) {
    super(props);
    bindThis(this);
    this.state = this.generateFreshState();
  }

  startWizard() {
    this.setState((): Object => {
      return {
        open: true,
      };
    });
  }

  /**
   * Returns the default state of WizardDisplay.
   */
  generateFreshState(): State {
    return {
      changed: false,
      open: false,
      submittedOpen: false,
      name: '',
      className: '',
      registry: '',
    };
  }

  /**
   * Creates the list of pages for the wizard.
   */
  generateWizardPages(): Array<Object> {
    return [
      this.createTextWizardPage('Name', 'What is the name of the starship'),
      this.createTextWizardPage('Class', 'What is the class of the starship', 'className'),
      this.createTextWizardPage('Registry', 'What is the registry of the starship'),
    ];
  }

  /**
   * Creates a wizard page with a single TextField element.
   * Automatically focuses TextField input when onFocus is called.
   *
   * @param {string} name The name of field. e.g. 'Year'
   * @param {string} explanation An explanation of the page.
   * @param {?string} propName The name of corresponding prop in the state and the id of the input. Defaults to the name in lower case.
   * @param {?function} onChange A function that handles changes to the value of input. Defaults to the pattern 'on' + $name + 'Change'.
   */
  createTextWizardPage(name: string,
                                           explanation: string,
                                           propName: string = (name.toLowerCase()),
                                           onChange: (event: Event) => void = this[`on${name}Change`]): Object {
    return {
      name,
      explanation,
      displayElement: (
          <div>
            <TextField
              hintText={name}
              floatingLabelText={name}
              floatingLabelFixed={true}
              value={this.state[propName]}
              onChange={onChange}
              id={propName}
            />
          </div>
      ),
      onFocus: () => {
        const input = document.getElementById(propName);
        if (input) {
          input.focus();
        }
      },
    };
  }

  onNameChange(event: Event, name: string) {
    this.setState((): Object => {
      return {
        name,
        changed: true,
      };
    });
  }

  onClassChange(event: Event, className: string) {
    this.setState((): Object => {
      return {
        className,
        changed: true,
      };
    });
  }

  onRegistryChange(event: Event, registry: string) {
    this.setState((): Object => {
      return {
        registry,
        changed: true,
      };
    });
  }

  onSubmit() {
    // close the dialog and open a new dialog showing submitted values
    this.setState((): Object => {
      return {
        open: false,
        submittedOpen: true,
      };
    });
  }

  onCancel() {
    this.setState((): Object => {
      return {
        open: false,
      };
    });
  }

  discardChanges() {
    this.setState((): State => {
      return this.generateFreshState();
    });
  }

  closeSubmittedDialog() {
    this.setState((): Object => {
      return this.generateFreshState();
    });
  }

  render(): React$Element<*> {
    let submittedDialog;
    if (this.state.submittedOpen) {
      submittedDialog = (
        <StyledDialog
          title="Wizard Finished"
          modal={true}
          open={true}
          onRequestClose={this.closeSubmittedDialog}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          actions={[]}
          closeIconInTitle={true}
          titleBackgroundColor={'#1E88E5'}
        >
          <div>
            <p>{`Name: ${this.state.name}`}</p>
            <p>{`Class: ${this.state.className}`}</p>
            <p>{`Registry: ${this.state.registry}`}</p>
          </div>
        </StyledDialog>
      );
    }
    return (
      <div className="wizard-display">
        <RaisedButton
          className="start-wizard-button"
          label="Start Wizard"
          primary={true}
          onClick={this.startWizard}
        />
        <Wizard
          type={'Starship'}
          changed={this.state.changed}
          pages={this.state.open ? this.generateWizardPages() : []}
          open={this.state.open}
          onComplete={this.onSubmit}
          onCancel={this.onCancel}
          discardChanges={this.discardChanges}
          name="newStarship"
          title="New Starship"
        />
        {submittedDialog}
      </div>
    );
  }
}
