import { connect } from 'react-redux';
import WizardDisplayComponent from './component';

const mapStateToProps = () => {
  return {
  };
};

const WizardDisplay = connect(mapStateToProps)(WizardDisplayComponent);
export default WizardDisplay;
