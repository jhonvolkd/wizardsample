/* globals document, window */
/* @flow */
import './App.less';
import React from 'react';
import ReactDOM from 'react-dom';
import WizardDisplay from './WizardDisplay';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from 'Reducers';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

window.onload = () => {
  const store = createStore(reducers);

  ReactDOM.render(
    <Provider store={store}>
      <MuiThemeProvider>
        <div className="wizard-sample">
          <h1>Starship Wizard Demo</h1>
          <WizardDisplay />
        </div>
      </MuiThemeProvider>
    </Provider>,
    document.getElementById('react-content'),
  );
};
