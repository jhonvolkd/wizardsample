const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const TARGET = process.env.npm_lifecycle_event;
const CommonDir = path.join(__dirname, 'app', 'Common');

const common = {
  context: path.join(__dirname),
  entry: {
    WizardSample: ['babel-polyfill', './app'],
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|server)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react'],
            plugins: ['transform-runtime', 'transform-flow-strip-types'],
          },
        },
      },
    ],
  },
  resolve: {
    alias: {
      Common: CommonDir,
      Actions: path.join(__dirname, 'app', 'Actions'),
      Reducers: path.join(__dirname, 'app', 'Reducers'),
    },
    extensions: ['.js', '.css', '.less'],
  },
};

if (TARGET === 'build-release') {
  module.exports = merge(common, {
    module: {
      rules: [
        {
          test: /\.less$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
              },
              {
                loader: 'less-loader',
                options: {
                  paths: [
                    path.resolve(CommonDir),
                  ],
                },
              },
            ],
          }),
        },
      ],
    },
    plugins: [
      new webpack.EnvironmentPlugin({
        NODE_ENV: 'production',
      }),
      new webpack.optimize.UglifyJsPlugin(),
      new ExtractTextPlugin('WizardSample.css'),
    ],
  });
} else if (TARGET === 'build-debug') {
  module.exports = merge(common, {
    devtool: 'source-map',
    output: {
      sourceMapFilename: '[name].js.map',
    },
    module: {
      rules: [
        {
          test: /\.less$/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
            },
            {
              loader: 'less-loader',
              options: {
                paths: [
                  path.resolve(CommonDir),
                ],
              },
            },
          ],
        },
        {
          enforce: 'pre',
          test: /\.js$/,
          exclude: /(node_modules|server)/,
          use: {
            loader: 'eslint-loader',
            options: {
              quiet: true,
            },
          },
        },
      ],
    },
  });
}
