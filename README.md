## To install:
* `npm install`

## To build:
* `npm run build-release`

## To run:
* `npm run run-server`

## [Vagrant](https://www.vagrantup.com/)
* `vagrant up`
* Access VM with `vagrant ssh`
* `cd wizardsample`
* Run `./setup_on_vm.sh`
* Open localhost:8080
