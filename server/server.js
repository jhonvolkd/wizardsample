/*
 * A local Koa server for showing off the WizardComponent.
 */

const Koa = require('koa');
const KoaStatic = require('koa-static');
const Router = require('koa-router');

let debug = false;

const port = 3000;

const app = new Koa();
const router = new Router();

setFlags(process.argv);

/**
 * Set any local variables that are determined by command-line flags
 */
function setFlags(args) {
  args.forEach((arg) => {
    switch (arg) {
      case '-d':
        debug = true;
        break;
      default:
        break;
    }
  });
}

function generateHTML(name) {
  return `
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>${name}</title>
      <link href="https://fonts.googleapis.com/css?family=Roboto:700,500,400,300,100" rel="stylesheet" type="text/css">
      ${!debug ? `<link href="${name}.css" rel="stylesheet" type="text/css">` : ''}
    </head>
    <body>
      <div id="react-content">
      </div>
      <script src="${name}.js"></script>
      ${debug ? `<script src="${name}.js.map"></script>` : ''}
    </body>
  </html>
  `;
}

router.get('/', (ctx) => {
  ctx.body = generateHTML('WizardSample');
});

// apply middleware
app.use(router.routes());
app.use(KoaStatic('build'));

app.listen(port);
console.log(`Now listening on port ${port}`);
