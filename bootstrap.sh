#!/bin/bash

# add nodesource repo
curl --silent https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
VERSION=node_7.x
DISTRO="$(lsb_release -s -c)"
echo "deb https://deb.nodesource.com/$VERSION $DISTRO main" | tee /etc/apt/sources.list.d/nodesource.list
echo "deb-src https://deb.nodesource.com/$VERSION $DISTRO main" | tee -a /etc/apt/sources.list.d/nodesource.list

apt-get update
apt-get install nodejs
cd /home/ubuntu/wizardsample
chmod +x setup_on_vm.sh
